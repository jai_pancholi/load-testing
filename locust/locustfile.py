# Run with
# locust --host=https://hiscox-convert.dfp.ai


from locust import HttpLocust, TaskSet, task

class UserBehavior(TaskSet):
    @task(1)
    def index(self):
        self.client.get('/')

    @task(2)
    def search(self):
        business_name = 'digital fineprint'
        postcode = 'se1 0hs'
        search_url = '/api/gateway/search?name={}&postcode={}'.format(business_name, postcode)
        self.client.get(search_url)


class WebsiteUser(HttpLocust):
    task_set = UserBehavior
    

