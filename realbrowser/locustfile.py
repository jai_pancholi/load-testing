# Run with

# export PATH=$PATH:/Users/jai/Sites/StressTesting/src/realbrowser
# locust --host=https://hiscox-convert.dfp.ai


from realbrowserlocusts import FirefoxLocust, ChromeLocust, PhantomJSLocust
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC


from locust import TaskSet, task


class LocustUserBehavior(TaskSet):

    def open_locust_homepage(self):
        self.client.get('https://hiscox-convert.dfp.ai')
        self.client.wait.until(EC.visibility_of_element_located((By.XPATH, '//*[@id="main-search"]/input[1]')), "search bar visible")

    def click_through_to_documentation(self):
        business = 'Digital Fineprint'
        postcode = 'SE1 0HS'

        self.client.find_element_by_xpath('//*[@id="main-search"]/input[1]').send_keys(business)
        self.client.find_element_by_xpath('//*[@id="main-search"]/input[2]').send_keys(postcode)
        self.client.find_element_by_xpath('//*[@id="main-search"]/input[3]').click()
        self.client.wait.until(EC.visibility_of_element_located((By.XPATH, '//*[@id="app"]/div/main/div/div[1]/div/section[4]')), "map is visible")

    @task(1)
    def homepage_and_docs(self):
        self.client.timed_event_for_locust("Go to", "homepage", self.open_locust_homepage)
        self.client.timed_event_for_locust("Click to", "search", self.click_through_to_documentation)


# class LocustUser(FirefoxLocust):
class LocustUser(ChromeLocust):
#class LocustUser(PhantomJSLocust):
    host = "not really used"
    timeout = 30 #in seconds in waitUntil thingies
    min_wait = 100
    max_wait = 1000
    screen_width = 1200
    screen_height = 600
    task_set = LocustUserBehavior